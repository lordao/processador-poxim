#pragma once

#include<stdint.h>
#include<stdio.h>
#include<stdlib.h>

const int32_t MEMORY_SIZE = 1024;

typedef uint8_t* memory_t;
typedef uint32_t word_t;

struct processor {
    word_t instructionRegister;
    word_t programCounter;
    word_t extensionRegister;
    word_t registers[32];
    word_t flagRegister;
};


typedef struct processor processor_t;

struct computer {
    memory_t memory;
    processor_t processor;
};

typedef struct computer computer_t;

computer_t createComputer() {
    computer_t computer = {
        .memory = (uint8_t*) calloc(MEMORY_SIZE, sizeof(uint8_t)),
        .processor = {
            .instructionRegister = 0,
            .programCounter = 0,
            .extensionRegister = 0,
            .flagRegister = 0
        }
    };
    return computer;
}

uint8_t readByte(computer_t *computer, uint32_t address) {
    if (computer == NULL || address >= MEMORY_SIZE) {
        return 0;
    }
    return computer->memory[address];
}

word_t readWord(computer_t *computer, uint32_t address) {
    address *= 4;
    if (computer == NULL || address >= MEMORY_SIZE) {
        return 0;
    }
    word_t word = 0;
    for (uint32_t i = address; i < address + 4; i++) {
        word = (word << 8) | readByte(computer, i);
    }
    return word;
}

void writeByte(computer_t *computer, uint32_t address, uint8_t value) {
    if (computer == NULL || address >= MEMORY_SIZE) {
        return;
    }
    computer->memory[address] = value;
}

void writeWord(computer_t *computer, uint32_t address, word_t value) {
    address *= 4;
    if (computer == NULL || address + 3 >= MEMORY_SIZE) {
        return;
    }
    uint8_t currentByte;
    for (int j = 3; j >= 0; j--) {
        /*
         * Imagine a palavra:
         * currentWord = 0x10203040 == 0b00010000 00100000 00110000 01000000
         *                             (espaços acima para fins didáticos)
         * O processo funcionará assim:
         *
         * j = 3:
         * 1. currentWord >> (8*3) == 0x10203040 >> 24 == 0x10
         * 2. 0x10 & 0xFF == 0x10 (máscara para um byte aplicada)
         * 3. currentByte = 0x10
         * 4. memória == [..., 0x10, X, X, X, ...]
         *
         * j = 2:
         * 1. currentWord >> (8*2) == 0x10203040 >> 16 == 0x1020
         * 2. 0x1020 & 0xFF == 0x20 (máscara para um byte aplicada)
         * 3. currentByte = 0x20
         * 4. memória == [..., 0x10, 0x20, X, X, ...]
         *
         * j = 1:
         * 1. currentWord >> (8*1) == 0x10203040 >> 8 == 0x102030
         * 2. 0x102030 & 0xFF == 0x30 (máscara para um byte aplicada)
         * 3. currentByte = 0x30
         * 4. memória == [..., 0x10, 0x20, 0x30, X, ...]
         *
         * j = 0:
         * 1. currentWord >> (8*0) == 0x10203040 >> 0 == 0x10203040
         * 2. 0x10203040 & 0xFF == 0x30 (máscara para um byte aplicada)
         * 3. currentByte = 0x40
         * 4. memória == [..., 0x10, 0x20, 0x30, 0x40, ...]
         *
         * Note que o número ficou ordenado do jeito esperado na memória. =)
         */
        currentByte = (value >> (8 * j)) & 0xFF;
        computer->memory[address++] = currentByte;
    }
}

word_t readNextInstruction(computer_t *computer) {
    if (computer == NULL) {
        return 0;
    }
    processor_t *p = &computer->processor;
    p->instructionRegister = readWord(computer, p->programCounter);
    p->programCounter++;
    return p->instructionRegister;
}

uint8_t getFlag(computer_t *computer, uint8_t offset) {
    if (computer == NULL) {
        return 0;
    }
    return (computer->processor.flagRegister >> offset) & 1;
}

uint8_t getEqualFlag(computer_t *computer) {
    return getFlag(computer, 0);
}

uint8_t getLowerThanFlag(computer_t *computer) {
    return getFlag(computer, 1);
}

uint8_t getGreaterThanFlag(computer_t *computer) {
    return getFlag(computer, 2);
}

uint8_t getZeroDivision(computer_t *computer) {
    return getFlag(computer, 3);
}

uint8_t getOverflow(computer_t *computer) {
    return getFlag(computer, 4);
}

void setFlag(computer_t *computer, uint8_t offset, uint8_t value) {
    if (computer == NULL) {
        return;
    }
    word_t flagRegister = computer->processor.flagRegister;
    if (value) {
        computer->processor.flagRegister = flagRegister | (1 << offset);
    } else {
        word_t mask = 1 << offset;
        computer->processor.flagRegister = flagRegister & ~mask;
    }
}

void setEqualFlag(computer_t *computer, uint8_t value) {
    return setFlag(computer, 0, value);
}

void setLowerThanFlag(computer_t *computer, uint8_t value) {
    return setFlag(computer, 1, value);
}

void setGreaterThanFlag(computer_t *computer, uint8_t value) {
    return setFlag(computer, 2, value);
}

void setZeroDivision(computer_t *computer, uint8_t value) {
    return setFlag(computer, 3, value);
}

void setOverflow(computer_t *computer, uint8_t value) {
    return setFlag(computer, 4, value);
}

word_t sOperand(word_t word) {
    return word & 0x03FFFFFF;
}

word_t* uOperands(word_t word) {
    word_t *operands = (word_t*) calloc(4, sizeof(word_t));
    // Extensão
    uint8_t ext, x, y, z;
    ext = (word >> 15) & 0x7;
    y = (ext & 1) << 5;
    x = ((ext >> 1) & 1) << 5;
    z = ((ext >> 2) & 1) << 5;

    // Rz
    operands[0] = (word >> 10) & 0x1F;
    operands[0] = operands[0] | z;
    // Rx
    operands[1] = (word >> 5) & 0x1F;
    operands[1] = operands[1] | x;
    // Ry
    operands[2] = word & 0b11111;
    operands[2] = operands[2] | y;

    return operands;
}

word_t* fOperands(word_t word) {
    word_t *operands = (word_t*) calloc(3, sizeof(word_t));
    // Imediato
    operands[0] = (word >> 10) & 0xFFFF;
    // Rx
    operands[1] = (word >> 5) & 0x1F;
    // Ry
    operands[2] = word & 0x1F;

    return operands;
}

void addi(computer_t *computer, word_t word) {
    word_t *operands = fOperands(word);
    word_t x = operands[1];
    word_t y = operands[2];
    word_t imediate = operands[0];
    word_t *rx = &computer->processor.registers[x];
    word_t *ry = &computer->processor.registers[y];
    uint64_t sum = *ry + imediate;
    *rx = sum;
    setOverflow(computer, sum >> 32);
    printf("FR = 0x%08X, R%d = R%d + 0x%04X = 0x%08X\n", computer->processor.flagRegister, x, y, imediate, *rx);
}

void mul(computer_t *computer, word_t word) {
    word_t *operands = uOperands(word);
    word_t x = operands[1];
    word_t y = operands[2];
    word_t z = operands[0];

    word_t *rx = &computer->processor.registers[x];
    word_t *ry = &computer->processor.registers[y];
    word_t *rz = &computer->processor.registers[z];

    uint64_t product = *rx * *ry;
    computer->processor.extensionRegister = product >> 32;
    setOverflow(computer, computer->processor.extensionRegister != 0);
    *rz = product & 0xFFFFFFFF;

    printf("mul r%d, r%d, r%d\n[U] ", z, x, y);
    printf("FR = 0x%08X, ER = 0x%08X, R%d = R%d * R%d = 0x%08X\n",
            computer->processor.flagRegister,
            computer->processor.extensionRegister,
            z, x, y,
            *rz);
}

void cmp(computer_t *computer, word_t word) {
    word_t *operands = uOperands(word);
    word_t x = operands[1];
    word_t y = operands[2];

    word_t *rx = &computer->processor.registers[x];
    word_t *ry = &computer->processor.registers[y];

    setEqualFlag(computer, *rx == *ry);
    setLowerThanFlag(computer, *rx < *ry);
    setGreaterThanFlag(computer, *rx > *ry);

    printf("cmp r%d, r%d\n[U] FR = 0x%08X\n", x, y, computer->processor.flagRegister);
}

void ldw(computer_t *computer, word_t word) {
    word_t *operands = fOperands(word);
    word_t x = operands[1];
    word_t y = operands[2];
    word_t imediate = operands[0];
    word_t *rx = &computer->processor.registers[x];
    word_t *ry = &computer->processor.registers[y];
    *rx = readWord(computer, *ry + imediate);
    printf("R%d = MEM[(R%d + 0x%04X) << 2] = 0x%08X\n", x, y, imediate, *rx);
}

void ldb(computer_t *computer, word_t word) {
    word_t *operands = fOperands(word);
    word_t x = operands[1];
    word_t y = operands[2];
    word_t imediate = operands[0];
    word_t *rx = &computer->processor.registers[x];
    word_t *ry = &computer->processor.registers[y];
    *rx = readByte(computer, *ry + imediate);
    printf("R%d = MEM[(R%d + 0x%04X)] = 0x%08X\n", x, y, imediate, *rx);
}

void stw(computer_t *computer, word_t word) {
    word_t *operands = fOperands(word);
    word_t x = operands[1];
    word_t y = operands[2];

    word_t imediate = operands[0];
    word_t *rx = &computer->processor.registers[x];
    word_t *ry = &computer->processor.registers[y];

    printf("stw r%d, ", operands[1]);
    if (operands[0] <= 1) {
        printf("%d", operands[0]);
    } else {
        printf("0x%04X", operands[0]);
    }
    printf(", r%d\n[F] ", operands[2]);

    writeWord(computer, *rx + imediate, *ry);
    printf("MEM[(R%d + 0x%04X) << 2] = R%d = 0x%08X\n", x, imediate, y, *ry);
}

void bun(computer_t *computer, word_t word) {
    word_t address = sOperand(word);
    printf("PC = 0x%08X\n", address * 4);
    computer->processor.programCounter = address;
}

void bConditional(computer_t *computer, word_t word, uint8_t condition) {
    if (condition) {
        bun(computer, word);
    } else {
        printf("PC = 0x%08X\n", computer->processor.programCounter*4);
    }
}

int8_t execute(computer_t *computer) {
    word_t nextInstruction = computer->processor.instructionRegister;
    uint8_t instruction = nextInstruction >> 26;
    int8_t retValue = 1;
    switch (instruction) {
        case 0x01:
            addi(computer, nextInstruction);
            break;
        case 0x04:
            mul(computer, nextInstruction);
            break;
        case 0x08:
            cmp(computer, nextInstruction);
            break;
        case 0x14:
            ldw(computer, nextInstruction);
            break;
        case 0x15:
            ldb(computer, nextInstruction);
            break;
        case 0x16:
            stw(computer, nextInstruction);
            break;
        case 0x1A:
            bun(computer, nextInstruction);
            break;
        case 0x1B:
            bConditional(computer, nextInstruction, getEqualFlag(computer));
            break;
        case 0x1C:
            bConditional(computer, nextInstruction, getLowerThanFlag(computer));
            break;
        case 0x1D:
            bConditional(computer, nextInstruction, getGreaterThanFlag(computer));
            break;
        case 0x1E:
            bConditional(computer, nextInstruction, !getEqualFlag(computer));
            break;
        case 0x1F:
            bConditional(computer, nextInstruction, getLowerThanFlag(computer) || getEqualFlag(computer));
            break;
        case 0x20:
            bConditional(computer, nextInstruction, getGreaterThanFlag(computer) || getEqualFlag(computer));
            break;
        case 0x3F:
            printf("CR = 0x00000000, PC = 0x00000000");
            retValue = 0;
            break;
        default:
            retValue = 0;
    }
    computer->processor.registers[0] = 0;
    return retValue;
}
