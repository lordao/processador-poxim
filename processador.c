#include<stdio.h>
#include<stdint.h>
#include "prettyPrint.h"
#include "processador.h"

void readFile(char *file, computer_t *computer) {
    FILE *handle = fopen(file, "r");
    int bufferSize = 40;
    char buffer[bufferSize];
    char *test;
    int i = 0;
    word_t currentWord;
    while (1) {
        test = fgets(buffer, bufferSize, handle);
        if (test == NULL) {
            break;
        }
        sscanf(buffer, "%X", &currentWord);
        writeWord(computer, i++, currentWord);
    }
}

void executionLoop(computer_t *computer) {
    printf("[START OF SIMULATION]\n");
    word_t nextInstruction;
    while (1) {
        nextInstruction = readNextInstruction(computer);
        printInstruction(nextInstruction);
        if (!execute(computer)) {
            break;
        }
    }
    printf("\n[END OF SIMULATION]\n");
}

int main(int argc, char **argv) {
    if (argc < 3) {
        return -1;
    }
    char *inputFile = argv[1];
    char *outputFile = argv[2];
    computer_t computer = createComputer();
    readFile(inputFile, &computer);
    executionLoop(&computer);
    return 0;
}
