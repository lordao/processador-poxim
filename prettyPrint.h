#include<stdio.h>
#include "processador.h"

void printByte(uint8_t byte) {
    printf("0x%02X\n", byte);
}

void printWord(word_t word) {
    printf("0x%08X\n", word);
}

void dumpMemory(computer_t *computer) {
    for (uint32_t i = 0; i < MEMORY_SIZE; i++) {
        printf("0x%04X: ", i*4);
        printWord(readWord(computer, i));
    }
}

void showProcessorState(computer_t *computer) {
    printf("PC: 0x%08X\n", computer->processor.programCounter);
    printf("ER: 0x%08X\n", computer->processor.extensionRegister);
    printf("IR: 0x%08X\n", computer->processor.instructionRegister);
    printf("FR: 0x%08X\n", computer->processor.flagRegister);
    printf("Rn: {\n");
    printf("  R0: 0x00000000\n");
    for (int i = 1; i < 32; i++) {
        printf(", R%d: 0x%08X\n", i, computer->processor.registers[i]);
    }
    printf("}\n");
}

char getType(word_t word) {
    uint8_t instruction = word >> 26;
    switch (instruction) {
        case 0x00:
        case 0x02:
        case 0x04:
        case 0x06:
        case 0x08:
        case 0x0A:
        case 0x0B:
        case 0x0C:
        case 0x0E:
        case 0x10:
        case 0x12:
            return 'U';
        case 0x01:
        case 0x03:
        case 0x05:
        case 0x07:
        case 0x09:
        case 0x0D:
        case 0x0F:
        case 0x11:
        case 0x13:
        case 0x14:
        case 0x15:
        case 0x16:
        case 0x17:
            return 'F';
        case 0x1A:
        case 0x1B:
        case 0x1C:
        case 0x1D:
        case 0x1E:
        case 0x1F:
        case 0x20:
        case 0x3F:
            return 'S';
    }
    return ' ';
}

char* instructionName(word_t word) {
    uint8_t instruction = word >> 26;
    switch (instruction) {
        case 0x00:
            return "add";
        case 0x01:
            return "addi";
        case 0x02:
            return "sub";
        case 0x03:
            return "subi";
        case 0x04:
            return "mul";
        case 0x05:
            return "muli";
        case 0x06:
            return "div";
        case 0x07:
            return "divi";
        case 0x08:
            return "cmp";
        case 0x09:
            return "cmpi";
        case 0x0A:
            return "shl";
        case 0x0B:
            return "shr";
        case 0x0C:
            return "and";
        case 0x0D:
            return "andi";
        case 0x0E:
            return "not";
        case 0x0F:
            return "noti";
        case 0x10:
            return "or";
        case 0x11:
            return "ori";
        case 0x12:
            return "xor";
        case 0x13:
            return "xori";
        case 0x14:
            return "ldw";
        case 0x15:
            return "ldb";
        case 0x16:
            return "stw";
        case 0x17:
            return "stb";
        case 0x1A:
            return "bun";
        case 0x1B:
            return "beq";
        case 0x1C:
            return "blt";
        case 0x1D:
            return "bgt";
        case 0x1E:
            return "bne";
        case 0x1F:
            return "ble";
        case 0x20:
            return "bge";
        case 0x3F:
            return "int";
    }
    return " ";
}

void printF(word_t word) {
    if (word >> 26 == 0x16) {
        return;
    }
    word_t *operands = fOperands(word);
    printf("%s r%d, r%d, ", instructionName(word), operands[1], operands[2]);
    if (operands[0] <= 1) {
        printf("%d", operands[0]);
    } else {
        printf("0x%04X", operands[0]);
    }
    printf("\n[F] ");
}

void printS(word_t word) {
    word_t operand = sOperand(word);
    printf("%s ", instructionName(word));
    if (operand <= 1) {
        printf("%d", operand);
    } else {
        printf("0x%08X", operand);
    }
    printf("\n[S] ");
}

void printInstruction(word_t word) {
    switch (getType(word)) {
        case 'S':
            printS(word);
            break;
        case 'F':
            printF(word);
            break;
    }
}
